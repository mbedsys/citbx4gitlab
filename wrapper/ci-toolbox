#!/usr/bin/env bash
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2017-2018 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# Copyright (C) 2018-2020 MBEDSYS - Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

# Print an error message and exit with error status 1
print_critical() {
    >&2 printf "\e[91m[CRIT] %s\e[0m\n" "$@" \
        "Additional debug information:" "${CITBX_DEBUG_INFO[@]}"
    exit 1
}

CITBX_DEBUG_INFO=()

# Add debug info
add_debug_info() {
    CITBX_DEBUG_INFO+=("$@")
}

CI_PROJECT_DIR="$(git rev-parse --show-toplevel 2>/dev/null || true)"

CITBX_SEARCH_PATH=${CITBX_SEARCH_PATH:-"tools/ci-toolbox:tools/gitlab-ci:/usr/local/lib/ci-toolbox:/usr/lib/ci-toolbox"}

_IFS=$IFS
IFS=':'
for d in $CITBX_SEARCH_PATH; do
    if [ "${d:0:1}" != "/" ]; then
        if [ -z "$CI_PROJECT_DIR" ]; then
            continue
        fi
        d="$CI_PROJECT_DIR/$d"
    fi
    if [ -e "$d/ci-toolbox.sh" ]; then
        CITBX_CI_TOOLBOX_PATH="$d/ci-toolbox.sh"
        break
    fi
    # Assure backward compatibility with CITBX_VERSION < 4
    if [ -e "$d/run.sh" ]; then
        CITBX_CI_TOOLBOX_PATH="$d/run.sh"
        break
    fi
    add_debug_info "ci-toolbox.sh or run.sh not found in directory $d"
done
IFS=$_IFS

if [ -z "$CITBX_CI_TOOLBOX_PATH" ]; then
    print_critical "Unable to find ci-toolbox.sh script" \
        " => You may have to update the CITBX_SEARCH_PATH to add the suitable ci-toolbox.sh relative directory from the project root" \
        "    e.g.: add export CITBX_SEARCH_PATH=\"$CITBX_SEARCH_PATH:your/custom/dir\" to your environment"
fi

# BASH COMP MODE: Check if the tool support bash completion
if [ -n "$CITBX_BASHCOMP" ]; then
    CITBX_VERSION=$(awk -F = '
        /^CITBX_VERSION=/ {
            print $2
        }
        NR > 50 {
            print 0
            exit
        }' $CITBX_CI_TOOLBOX_PATH 2> /dev/null || true)
    # skip bash completion for CITBX_VERSION < 3.x.x
    test ${CITBX_VERSION%%.*} -ge 3 || \
        grep -m 1 CITBX_BASHCOMP -q $CITBX_CI_TOOLBOX_PATH \
        || exit
fi

export CITBX_TOOL_NAME=ci-toolbox
exec $CITBX_CI_TOOLBOX_PATH "$@"
print_critical "Unable to run $CITBX_CI_TOOLBOX_PATH tool"
