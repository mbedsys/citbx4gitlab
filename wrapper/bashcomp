#!/usr/bin/env bash
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2017-2018 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# Copyright (C) 2018-2020 MBEDSYS - Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

_ci_toolbox_compgen() {
    local cur prev cmd result line
    _init_completion -s || return
    cur=${COMP_WORDS[COMP_CWORD]}
    cmd=$(eval echo "${COMP_WORDS[1]}")
    if [ $COMP_CWORD -eq 1 ]; then
        result=$(compgen -W "$(CITBX_BASHCOMP=commands ci-toolbox 2>/dev/null || true)" -- "$cur")
    else
        case "$cur" in
            -*)
                result=$(compgen -W "$(CITBX_BASHCOMP=opts ci-toolbox "$cmd" 2>/dev/null || true)" -- "$cur")
                ;;
            --*)
                result=$(compgen -W "$(CITBX_BASHCOMP=longopts ci-toolbox "$cmd" 2>/dev/null || true)" -- "$cur")
                ;;
            *)
                prev=${COMP_WORDS[$(($COMP_CWORD - 1))]}
                case "$prev" in 
                    -*)
                        result=$(compgen -W "$(CITBX_BASHCOMP=$prev ci-toolbox "$cmd" 2>/dev/null || true)" -- "$cur")
                        if [ -z "$result" ]; then
                            _filedir
                            return 0
                        fi
                        ;;
                esac
                ;;
        esac
    fi
    if [ -n "$result" ]; then
        COMPREPLY=()
        while read -r line; do
            COMPREPLY+=("$(printf "%q\n" "$line")")
        done <<< "$result"
    fi
    return 0
} &&
complete -F _ci_toolbox_compgen ci-toolbox
