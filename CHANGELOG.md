# Changelog

## 8.4.2
* [choose] Fix menu display with one element

## 8.4.1
* [choose] Fix menu display when the menu size is greater than the window height

## 8.4.0
* [ci-toolbox] Add choose menu to select a job and environment

## 8.3.1
* [gitlabci_yml2bash/include] Display an error when file not found
* [.ci-toolbox.properties] Remove deprecated parameter
* [CI] Auto generate `RELEASE.md` file from the `CHANGELOG.md` file

## 8.3.0
* [CI] Update
* [ci-toolbox] Add parallel/matrix support

## 8.2.0
* Remove pipeline command
* Remove cli argument settings

## 8.1.1
* [ci-toolbox] Fix job file processing

## 8.1.0
* [ci-toolbox] add `CITBX_JOB_DEPENDENCY_STRATEGY` option
* [tests] Add test case for `CITBX_JOB_DEPENDENCY_STRATEGY` option add

## 8.0.1
* [FIX] Command and entrypoint treatment regression since python parser introduction

## 8.0.0
* Update build-package.sh reuse variable in sed command
* [ci/scripts/jobs/build-package] Add release file sanity check
* [tests/run-testsuite] Minor updates
* [gitlabci_yml2bash] Add `inherit` keyword support
* [gitlabci_yml2bash] Add support of local include with wildcard
* [tests] Add yaml2bash tests

## 7.3.1
* [setup] Remove useless functions
* [ci-toolbox] Fix: source env dir after git submodule update

## 7.3.0
* [ci-toolbox/gitlabci_yml2bash] Add deprecated keyword warning
* [ci-toolbox] Sort job list by stage in the help summary
* [CI] Store packages into the package registry
* [setup] Setup part reworking

## 7.2.0
* [ci-toolbox] Rewrite job script execution
* [ci-toolbox] Fix `CI_COMMIT_*` variable fill with special characters

## 7.1.0
* [ci-toolbox] Call citbx_source_env_dir after GitLab-CI YAML file eval
* [ci-toolbox] change current dir to  before running after_script part
* [gitlabci_yml2bash] Print each command before executing it

## 7.0.0
* [gitlab-ci-yml2bash] Job process rewrite
* [gitlab-ci-yml2bash] Add CI_JOB_STAGE export
* [ci-toolbox] export to the job all CI_xxx environment variables
* [gitlab-ci-yml2bash] Minor update on error message
* [gitlab-ci-yml2bash] Fix case where a job has no stage defined to return the corresponding error message
* [gitlab-ci-yml2bash] Introduce support of template include and file include from other projets
* [ci-toolbox] Update 'update' command help
* [ci-toolbox] Add GitLab CI_xxx env variables support related to the Git repository
* [ci-toolbox] Add GitLab CI_JOB_IMAGE env variable
* [ci-toolbox] Add GitLab CI_JOB_STATUS env variable support in after_script parts
* [yml2bash] Improve python coding style and add pylint check in the test job

## 6.3.0
* Set CI script path to ci/scripts
* [gitlab-ci-yml2bash] Update 'Undefined template' error message
* [gitlab-ci-yml2bash] Add error when a stage is undefined
* [ci-toolbox] Get real path when running a shell
* [README] Update install part

## 6.2.0
* [ci-toolbox] Fix 'extends' functionality to be aligned to the correct Gitlab-CI functioning
* [CI] Use extends Gitlab-CI keyword instead of YAML template notations

## 6.1.0
* [ci-toolbox] Remove docker-registry option and ensure that CI_REGISTRY variable is available on time

## 6.0.2
* [gitlab-ci-yml2bash] Fix variable evaluation in some fields like 'image'

## 6.0.1
* [setup] Improve setup part

## 6.0.0
* [ci-toolbox] Rewrite YAML files processing in python
* [ci-toolbox] Remove deprecated citbx_run_ext_job frunction
* [ci-toolbox] Remove docker-prune command

## 5.10.0
* [setup] Rework setup process
* [tests] Update test job

## 5.9.1
* [ci-toolbox] Fix CITBX_DEBUG_SCRIPT_ENABLED option description
* [ci-toolbox] Avoid uid conflict
* [submodule] Move ci-scripts to a git submodule
* [3rdparty/bashopts] Upgrade bashopts
* [ci-toolbox] Update git dir local variable name

## 5.9.0
* Copyright update
* [ci-toolbox] Add CITBX_PROJECT_ENV for project specific env

## 5.8.2
* [fix] Update CITBX_PIPELINE_NAME short option to avoid conflict with non-interactive mode option

## 5.8.1
* [ci-toobox] Fix CI_REGISTRY_IMAGE env var evaluation

## 5.8.0
* [ci-toolbox] Add CI_REGISTRY_IMAGE variable

## 5.7.0
* [ci-toolbox] Add CI_PROJECT_NAME, CI_PROJECT_NAMESPACE and CI_PROJECT_PATH variables

## 5.6.0
* [ci-toolbox] Update the default setup component list
* [ci-toolbox] Add update command

## 5.5.0
* [ci-toolbox] Add CI_JOB_STAGE environment variable

## 5.4.0
* [ci-toolbox] Add --job-token option

## 5.3.4
* [executor/shell] Remove backtrace on job script execution failure
* [executor/docker/services] Fix service start with empty command
* [CI] Upgrade distro images and test suite

## 5.3.3
* [ci-toolbox] Update yaml2json to use yaml.safe_load

## 5.3.2
* [ci-toolbox] Fix job list fetch

## 5.3.1
* Update project namespace
* [ci-toolbox] Allow script in string format in addition to array

## 5.3.0
* [ci-toolbox] Update ci-toolbox.sh CITBX_VERSION from VERSION file
* [wrapper] Improve bash completion mode detection when the local ci-toolbox/ci-toolbox.sh file is used (useful during development process)
* [bashopts] Update version with the fix of enumeration type to support values with spaces
* [ci-toolbox] Move job run part into citbx_run_job
* [ci-toolbox] Rewrite Gitlab-CI file treatment parts like image, services and scripts objects
* [ci-toolbox] Add pipeline feature
* [tests] Add the test suite (tests/run-testsuite.sh)

## 5.2.1
* [ci-toolbox] Fix CITBX_VERSION_REQ_MIN test to prevent recursive 'ci-toolbox setup' call
* [wrapper] Improve existing file detection (file command not enough reliable)

## 5.2.0
* [job-test-services] Add timeout
* [ci-toolbox] deprecate citbx_run_ext_job function
* [ci-toolbox] Add CITBX_RUN_JOB_DEPENDENCIES job property (--with-dependencies option) to run dependencies before
* [ci-toolbox] Simplify YAML values read and update a message

## 5.1.0
* [CI] add experimental branch to the package release template
* [ci-scripts] Fix typo in a message display
* Variable rename: CITBX_JOB_RUN_FILE_NAME => CITBX_JOB_FILE_NAME
* [ci-toolbox] Improving the job variable management
* [ci-toolbox] Check if a service is started
* [job-advanced] Update job-advanced example job:
    + Remove citbx_run_ext_job call
    + Add test to check YAML variable scope
* [ci-toolbox] Use new command 'docker image ls' instead of legacy 'docker images' one
* [ci-toolbox] Remove useless check_tool_name function
* [ci-toolbox] Unset CITBX_RUN_SHELL in citbx_run_ext_job
* [ci-toolbox] Fix command check (job list part)
* [ci-toolbox] Replacing useless legacy CITBX_TOOL_NAME variable by the fixed 'ci-toolbox' value
* [ci-toolbox] Add citbx_ name prefix for several internal functions
* [ci-toolbox] Add a way to check minimal tool version for certain projects

## 5.0.0
* [ci-toolbox] PATH environment variable propagation - Fix regression from the version 4.1.0
* Project split into two independent parts: ci-toolbox and run-job-script
* Switch to the version Python 3 of YAML package
* [env-setup/gentoo] Fix git-lfs component setup
* [ci-toolbox] Simplify and improve the reliability the job script part
* [ci-toolbox] Add ability to run ci-toolbox outside a terminal
* [env-setup/fedora] Add Fedora support
* [ci-toolbox] Add option to enable access to the host docker daemon into the job environment
* [modules/runshell] Add runshell module to run a shell on a specific build step, useful for debugging
* [ci-toolbox] Add options to manage Docker configuration propagation
* [packaging] Add build-package job for Debian-like and RedHat-like operating systems
* [CI] Add testsuites for RPM and DEB packages

## 4.1.0
* [env-setup/common] update install_tools function
* [setup/upgrade] Add a migration tool from version 2 and 3 to 4 and upper
* [ci-toolbox] Add OS_RELEASE_INFO hashmap
* [ci-toolbox] Fix in docker script part to be more portable on distribution like Fedora
* [wrapper] Move wrapper directory outside of tools/ci-toolbox
* [ci-toolbox] Increase portability by using /usr/bin/env in shebang
* [README] README files update

## 4.0.0
* [README] Add French version
* [LICENSE] Remove instruction section at the end
* [CI] Add docker tag
* [ci-toolbox/setup] Add --os-id option
* [ci-toolbox/git-lfs] Remove CITBX_GIT_LFS_ENABLED option and git lfs pull no more useful
* [ci-toolbox] Add support of ci-toolbox.sh stored outside project repo
    + Rename script run.sh to ci-toolbox.sh
    + Rename directories run.d => jobs and gitlab-ci => ci-toolbox
    + Run jobs from project containing .gitlab-ci.yml without having to integrate ci-toolbox into this one
    + Update ci-toolbox setup an update process
* [ci-toolbox] Fix colors display with some shells like dash
* [ci-toolbox] Remove CITBX_WAIT_FOR_SERVICE_START option
* [ci-toolbox] Add docker-prune command
* [ci-toolbox] set CITBX_DOCKER_LOGIN_MODE default value to disabled
* [ci-toolbox] Set non interactive mode as default mode
* [ci-toolbox] Rewrite CITBX_COMMANDS and CITBX_DOCKER_SCRIPT for better readability and compatibility with gitlab-runner operating

## 3.3.1
* [executor/docker] Add support to busybox based image and use addgroup instead of usermod
* [FIX] broken --docker-login=enabled & improve contextual information message
* [module/dockerimg] file moved to ercom/docker project
* [CA] Fill CI_SERVER_TLS_CA_FILE with local CA certs on local worstation

## 3.3.0
* [env-setup] Improve ca-certificates setup part
* [modules/dockerimg] Update dockerimg module
* [modules/example] minor fix on variable export
* Remove ci-job-wrapper jobs and update the documentation
* Update docker-login option

## 3.2.0
* [env-setup] Modular setup
  + add ability to limit setup on specified elements
  + add ci-tool setup
  + add gentoo support

## 3.1.0
* [FIX/bashcomp] Fix number calculation in the _citbx4gitlab_compgen function
* Add disable-service parameter (CITBX_DISABLED_SERVICES) to disable specified services

## 3.0.1
* [FIX] regression: CITBX_TOOL_NAME not defined when tools/gitlab-ci/run.sh is called directly

## 3.0.0
* Add support of bash completion
* [FIX] remove warning from a test
* [FIX] unset CITBX_GIT_CLEAN for citbx_run_ext_job
* [CI] add job-test-services-mysql and job-test-services-postgres service example jobs
* Update docker run to use CITBX_JOB_SHELL
* [FIX] YAML variable treatment
* Improve messages/help formating
* Add debug-script option
* Remove deprecated JOB_EXT_FILE_NAME variable name
* [3rd] Update bashopts to the version 2.0.0
* Update fetch_file function for update command
* [FIX] docker run exit code on error
* [FIX] Preserve PATH environment variable in user mode
* Add VERSION and a CHANGELOG.md

## 2.2.10
* [FIX] unset CITBX_JOB_RUN_FILE_NAME in citbx_run_ext_job

## 2.2.9
* Improve git worktree support and preserve the working directory on shell mode

## 2.2.8
* Update variable name: CITBX_JOB_RUN_FILE_NAME

## 2.2.7
* Add ncore function

## 2.2.6
* [env-setup/ubuntu] Fix GIT LFS repository

## 2.2.5
* [env-setup/ubuntu] Update GIT LFS apt repository add

## 2.2.4
* Set git lfs as an optional feature

## 2.2.3
* [run] improve docker-dns detection

## 2.2.2
* [env-setup/ubuntu] [Fix] ca-certificates copy
* [Fix] read empty or invalid daemon.json

## 2.2.1
* [env-setup/ubuntu] Add --allow-change-held-packages option to apt-get remove docker.io & docker-engine
* [run] improve CI PROJECT ROOT DIR detection

## 2.2
* [FIX] service-privileged property variable name update
* Add GIT LFS support
* Improve docker-dns option check

## 2.1.1
* [FIX] service with custom commands with default entrypoint
* Add option to start service in privileged mode

## 2.1
* Add git-clean and group options

## 2.0.2
* [FIX] Eval YAML service properties

## 2.0.1
* [FIX] export variables to services

## 2.0
* Add Gitlab services support

## 1.3.2
* [fix] Prevent multiple call of citbx_job_finish

## 1.3.1
* add SIGINT SIGTERM to citbx_job_finish trap callback

## 1.3.0
* [fix] return the good error code on local job execution failure
* Add citbx_docker_run_add_args function

## 1.2.1
* [3rd] Update bashopts to the version 1.3.0
* Add citbx_export function
* [fix] Variable export the SHELL executor

## 1.2
* Changes:
* Add a check on the bash version
* Add submodule-strategy option for GIT_SUBMODULE_STRATEGY
* Add job executor option
* Add job script and job main func check
* Add image entrypoint support
* Remove CITBX_DOCKER_USER option
* reset PWD to CI_PROJECT_DIR before hooks execution
* [FIX] ubuntu setup
* Update the documentation

## 1.1
* Add CITBX_DEFAULT_JOB_SHELL property and CITBX_JOB_SHELL option
* Add job execution time
* Add update tool to fetch the last version
* Add gitlab GIT_SUBMODULE_STRATEGY support

## 1.0
* First stable release
