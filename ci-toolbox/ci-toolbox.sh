#!/usr/bin/env bash
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2017-2018 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# Copyright (C) 2018-2021 MBEDSYS - Emeric Verschuur <emeric@mbedsys.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CITBX_VERSION=$(cat $(dirname $0)/../VERSION)
CITBX_TOOL_DIR=$(dirname $(readlink -f $0))
if [ -f "$CITBX_TOOL_DIR/ci-toolbox.properties" ]; then
    . "$CITBX_TOOL_DIR/ci-toolbox.properties"
fi
. "$CITBX_TOOL_DIR/common.sh"

citbx_env_init

# Check minimum version if required
if [ -n "$CITBX_VERSION_REQ_MIN" ] && [ "$1" != "setup" ] \
    && [ $(printf "%d%04d%04d" ${CITBX_VERSION//[^0-9]/ }) \
        -lt $(printf "%d%04d%04d" ${CITBX_VERSION_REQ_MIN//[^0-9]/ }) ]; then
    print_warning "This project require CI-Toolbox version $CITBX_VERSION_REQ_MIN or greater" \
                "This tool will be upgraded to the last available version" \
                "You can abort this process and manually update this tool using 'ci-toolbox setup --component ci-toolbox <specific version>"
    echo -n "Type [ENTER] to continue or [CTRL]+[C] to abort."
    read
    exec ci-toolbox setup --component ci-toolbox
    exit 1
fi

# Collect OS release information
declare -A OS_RELEASE_INFO
eval "$(sed -E 's/^([^=]+)=(.*)$/OS_RELEASE_INFO[\1]=\2/g' /etc/os-release)"

# Collect the missing binaries and other dependencies
CITBX_MISSING_PKGS=()
for bin in gawk jq dockerd; do
    if ! which $bin > /dev/null 2>&1; then
        CITBX_MISSING_PKGS+=($bin)
    fi
done
if [ "$(echo "true" | python3 -c 'import sys, yaml, json; json.dump(yaml.safe_load(sys.stdin), sys.stdout)' 2>/dev/null)" != "true" ]; then
    CITBX_MISSING_PKGS+=("python-yaml")
fi
if [ "$CITBX_GIT_LFS_SUPPORT_ENABLED" == "true" ] && ! git lfs version > /dev/null 2>&1; then
    CITBX_MISSING_PKGS+=("git-lfs")
fi

# Check environment and run setup
citbx_check_env() {
    if [ "$1" != "true" ]; then
        if [ ${#CITBX_MISSING_PKGS[@]} -gt 0 ]; then
            print_critical "System setup needed (binary(ies)/component(s) '${CITBX_MISSING_PKGS[*]}' missing): please execute 'ci-toolbox setup' first"
        fi
        return 0
    fi
    local setupsh="$CITBX_TOOL_DIR/env-setup/$CITBX_SETUP_OS_ID.sh"
    if [ ! -f "$setupsh" ]; then
        print_critical "OS variant '$CITBX_SETUP_OS_ID' not supported. You can:" "* Write the suitable setup script $setupsh" \
                        "* Use --os-id option to use setup script from an other similar OS"
    fi
    . "$setupsh"
    print_info "System setup complete" "On a first install, a system reboot may be necessary"
    exit 0
}

# Get the job list
citbx_job_list() {
    local prefix outcmd arg
    outcmd='print $0'
    if ! arglist=$(getopt -o "f:p:s" -n "citbx_list " -- "$@"); then
        print_critical "Usage citbx_list: [options]" \
            "        -f <val>  Output gawk command (default: 'print $0')" \
            "        -s        Suffix list (same as -f 'printf(\" %s\", f[1]);')" \
            "        -p <val>  Prefix string"
    fi
    eval set -- "$arglist";
    while true; do
        arg=$1
        shift
        case "$arg" in
            -f) outcmd=$1;  shift;;
            -p) prefix=$1;  shift;;
            -s) outcmd='printf(" %s", f[1]);';;
            --) break;;
            *)  print_critical "Fatal error";;
        esac
    done
    printf "%s\n" "${CITBX_JOB_LIST[@]}" \
        | gawk 'match($0, /^'"$prefix"'(.*)$/, f) {'"$outcmd"'}'
}

# Run job dependencies
citbx_run_job_dependencies() {
    for v in "${CITBX_SUBJOB_ENV_EXPORT_LIST[@]}"; do
        env_args+=("$v=${!v}")
    done
    print_info "Number of dependencies: ${#CITBX_JOB_DEPENDENCIES_LIST[@]}"
    for j in "${CITBX_JOB_DEPENDENCIES_LIST[@]}"; do
        env "${env_args[@]}" $0 "$j"
    done
    print_info "All dependency jobs executed"
}

# Export a variable to the job environment
citbx_export() {
    CITBX_ENV_EXPORT_LIST+=("$@")
}

# Export a variable to the sub jobs
citbx_subjob_export() {
    CITBX_SUBJOB_ENV_EXPORT_LIST+=("$@")
}

# Add docker run arguments
citbx_docker_run_add_args() {
    CITBX_JOB_DOCKER_RUN_ARGS+=("$@")
}

citbx_choose() {
    if [ ! -v CHOOSE_ADD_OPTS ]; then
        CHOOSE_ADD_OPTS=("-s")
    fi
    bash -e "$CITBX_TOOL_DIR/3rdparty/choose.sh" "${CHOOSE_ADD_OPTS[@]}" -l : "$1" "$@"
}

citbx_run_job() {
    if [ -n "$CITBX_JOB_PARALLEL_MATRIX_COUNT" ] && [ $CITBX_JOB_PARALLEL_MATRIX_ID -eq 0 ]; then
        print_info "This job contains a parallel/matrix node." \
            "=> You can run this job with an additional parameter (-M|--matrix <environment number>)" \
            "with a number from this environment list:"
        printf "%s\n" "${CITBX_JOB_PARALLEL_MATRIX_LIST[@]}"
        print_info "...or, select this environment from this list:"
        local i menu_list=()
        for i in $(seq 1 ${#CITBX_JOB_PARALLEL_MATRIX_LIST[@]}); do
            menu_list+=("$i:Environment $i")
        done
        CITBX_JOB_PARALLEL_MATRIX_ID="$(citbx_choose "${menu_list[@]}")"
    fi
    citbx_eval_gitlab_ci CI_JOB_NAME
    if [ "$CITBX_JOB_EXECUTOR" == "auto" ]; then
        if [ -n "$CITBX_JOB_IMAGE_NAME" ]; then
            CITBX_JOB_EXECUTOR="docker"
        else
            CITBX_JOB_EXECUTOR="shell"
        fi
    fi
    if [ -n "$CITBX_JOB_EXPLICIT_DEPS_ONLY" ]; then # DEPRECATED
        print_warning 'Deprecated use of variable $CITBX_JOB_EXPLICIT_DEPS_ONLY' \
                    ' => please use $CITBX_DEFAULT_JOB_DEPENDENCY_STRATEGY variable instead'
        if [ "$CITBX_JOB_EXPLICIT_DEPS_ONLY" == "true" ]; then
            CITBX_JOB_DEPENDENCY_STRATEGY="needs"
        else
            CITBX_JOB_DEPENDENCY_STRATEGY="default"
        fi
    fi
    # Fetch git submodules
    if [ "$GIT_SUBMODULE_STRATEGY" != "none" ]; then
        GIT_SUBMODULE_ARGS=()
        case "$GIT_SUBMODULE_STRATEGY" in
            normal)
                ;;
            recursive)
                GIT_SUBMODULE_ARGS+=("--recursive")
                ;;
            *)
                print_critical "Invalid value for GIT_SUBMODULE_STRATEGY: $GIT_SUBMODULE_STRATEGY"
                ;;
        esac
        print_info "Fetching git submodules..."
        git submodule --quiet sync "${GIT_SUBMODULE_ARGS[@]}"
        git submodule update --init "${GIT_SUBMODULE_ARGS[@]}"
    fi

    if [ "$CITBX_GIT_CLEAN" == "true" ]; then
        git clean -fdx
        if [ "$GIT_SUBMODULE_STRATEGY" != "none" ]; then
            git submodule --quiet foreach "${GIT_SUBMODULE_ARGS[@]}" git clean -fdx
        fi
    fi

    citbx_source_env_dir

    # Run job dependencies first (if asked)
    if [ "$CITBX_RUN_JOB_DEPENDENCIES" == "true" ]; then
        citbx_run_job_dependencies
    fi

    # Login to the registry if needed
    if [ -n "$CI_REGISTRY" ] \
        && ( ( [ -z "$(jq -r '."auths"."'$CI_REGISTRY'" | select(.auth != null)' $HOME/.docker/config.json 2> /dev/null)" ] \
                && [ "$CITBX_DOCKER_LOGIN_MODE" == "auto" ] ) \
            || [ "$CITBX_DOCKER_LOGIN_MODE" == "enabled" ] ); then
        print_info "You seem to be not authenticated against the gitlab docker registry" \
            "> You can disable this feature by using --docker-login=disabled" \
            "> Or force this feature permanently by setting CITBX_DEFAULT_DOCKER_LOGIN_MODE into $CI_PROJECT_DIR/.ci-toolbox.properties" \
            "Please enter your gitlab user id and password:"
        docker login $CI_REGISTRY
    fi

    # Compute commands from before_script script and after_script
    # ### Script part: BEGIN ###
    CITBX_JOB_SCRIPT_PARTS+=('
        set -eo pipefail
        print_info() {
            printf "\033[1m\033[92m%s\033[0m\n" "$@"
        }
        print_error() {
            printf "\033[1m\033[91m%s\033[0m\n" "$@"
        }
        ')
    if [ "$CITBX_DEBUG_SCRIPT_ENABLED" == "true" ]; then
        CITBX_JOB_SCRIPT_PARTS+=('SHELL_CMD="$SHELL -e -x -c"')
    else
        CITBX_JOB_SCRIPT_PARTS+=('SHELL_CMD="$SHELL -e -c"')
    fi
    # ### Script part ###
    CITBX_JOB_SCRIPT_PARTS+=('
        run_scripts() {
            local __citbx_job_exit_code__=0
            $SHELL_CMD '"$CITBX_JOB_SCRIPT"' || __citbx_job_exit_code__=$?
            if [ $__citbx_job_exit_code__ -eq 0 ]; then
                export CI_JOB_STATUS=success
            else
                export CI_JOB_STATUS=failure
            fi
    ')
    if [ -v CITBX_JOB_AFTER_SCRIPT ]; then
        CITBX_JOB_SCRIPT_PARTS+=('
            print_info "Running after script..."
            $SHELL_CMD '"$CITBX_JOB_AFTER_SCRIPT"' || true
        ')
    fi
    CITBX_JOB_SCRIPT_PARTS+=('
            if [ $__citbx_job_exit_code__ -eq 0 ]; then
                print_info "Job succeeded"
            else
                print_error "ERROR: Job failed: exit code $__citbx_job_exit_code__"
            fi
            return $__citbx_job_exit_code__
        }
        run_scripts
    ')
    CITBX_JOB_SCRIPT="'${CITBX_JOB_SCRIPT_PARTS[*]//\'/\'\\\'\'}'"

    # If not set, fill the CI_SERVER_TLS_CA_FILE with local CA certificates
    if ! [[ -v CITBX_TLS_CA_SEARCH_DIR_LIST ]]; then
        CITBX_TLS_CA_SEARCH_DIR_LIST=("/usr/local/share/ca-certificates/")
    fi
    if ! [[ -v CI_SERVER_TLS_CA_FILE ]]; then
        CI_SERVER_TLS_CA_FILE="$(
            for dir in "${CITBX_TLS_CA_SEARCH_DIR_LIST[@]}"; do
                test ! -d "$dir" \
                    || find "$dir" -iregex '.*\.\(pem\|crt\)$' -exec openssl x509 -in '{}' \;
            done
        )"
    fi

    # Get GIT info
    : ${CITBX_REMOTE_NAME:=origin}
    CI_COMMIT_REF_NAME="$(git -C "$CI_PROJECT_DIR" rev-parse --abbrev-ref HEAD)"
    CI_COMMIT_REF_SLUG="$(sed 's/[A-Z]/\l&/g; s/[^a-z0-9]/-/g' <<< "$CI_COMMIT_REF_NAME")"
    CI_COMMIT_SHA="$(git -C "$CI_PROJECT_DIR" rev-parse --verify HEAD)"
    CI_COMMIT_SHORT_SHA="$(git -C "$CI_PROJECT_DIR" rev-parse --short  --verify HEAD)"
    CI_COMMIT_AUTHOR="$(git show -s --format='%an <%ae>')"
    CI_COMMIT_TITLE="$(git show -s --format='%s')"
    CI_COMMIT_MESSAGE="$(git show -s --format='%s%n%b')"
    CI_COMMIT_DESCRIPTION="$(git show -s --format='%b')"
    CI_COMMIT_TIMESTAMP="$(git show -s --format='%aI')"
    CI_DEFAULT_BRANCH="$(git symbolic-ref refs/remotes/$CITBX_REMOTE_NAME/HEAD >/dev/null 2>&1 \
                                || true | sed 's@^refs/remotes/'"$CITBX_REMOTE_NAME"'/@@')"
    citbx_export CI_COMMIT_AUTHOR       \
                 CI_COMMIT_DESCRIPTION  \
                 CI_COMMIT_MESSAGE      \
                 CI_COMMIT_REF_NAME     \
                 CI_COMMIT_REF_SLUG     \
                 CI_COMMIT_SHA          \
                 CI_COMMIT_SHORT_SHA    \
                 CI_COMMIT_TITLE        \
                 CI_COMMIT_TIMESTAMP    \
                 CI_DEFAULT_BRANCH       

    citbx_export "${CITBX_JOB_VARIABLE_LIST[@]}"

    CI_JOB_IMAGE=$CITBX_JOB_IMAGE_NAME
    citbx_export CI_JOB_IMAGE

    eval "$(git remote -v | gawk '
        $1 == "'"$CITBX_REMOTE_NAME"'" {
            printf("CI_REPOSITORY_URL=\"%s\"\n", $2);
            gsub(/^.*:[:/]*/, "", $2);
            gsub(/\.git$/, "", $2);
            printf("CI_PROJECT_PATH=\"%s\"\n", $2);
            exit;
        }
    ')"
    if [ -n "$CI_PROJECT_PATH" ]; then
        CI_PROJECT_PATH_SLUG="$(sed 's/[A-Z]/\l&/g; s/[^a-z0-9]/-/g' <<< "$CI_PROJECT_PATH")"
        CI_PROJECT_NAME=${CI_PROJECT_PATH##*/}
        CI_PROJECT_NAMESPACE=${CI_PROJECT_PATH%/*}
        CI_PROJECT_ROOT_NAMESPACE=${CI_PROJECT_PATH%%/*}
        CI_REGISTRY_IMAGE="$CI_REGISTRY/$CI_PROJECT_PATH"
        citbx_export CI_PROJECT_PATH            \
                     CI_PROJECT_PATH_SLUG       \
                     CI_PROJECT_NAME            \
                     CI_PROJECT_NAMESPACE       \
                     CI_PROJECT_ROOT_NAMESPACE  \
                     CI_REGISTRY_IMAGE          \
                     CI_REPOSITORY_URL
    fi

    # Add variable to the environment list
    citbx_export $(awk 'BEGIN {
            for(v in ENVIRON) {
                if (v ~ /^CI_/) {
                    print v;
                }
            }
        }')                             \
                 CI_JOB_NAME            \
                 CI_JOB_TOKEN           \
                 CI_JOB_STAGE           \
                 CI_PROJECT_PATH        \
                 CI_PROJECT_NAME        \
                 CI_PROJECT_NAMESPACE   \
                 CI_REGISTRY            \
                 CI_REGISTRY_IMAGE      \
                 CI_PROJECT_DIR         \
                 CI_SERVER_TLS_CA_FILE  \
                 CITBX_SCRIPTS_DIR      \
                 CITBX_MODULES_DIR      \
                 CITBX_JOBS_DIR         \
                 "${CITBX_PROJECT_ENV[@]}"

    if [ "$CITBX_DEBUG_SCRIPT_ENABLED" == "true" ]; then
        citbx_before_script="set -x"
        citbx_after_script="set +x"
    else
        citbx_before_script=""
        citbx_after_script=""
    fi

    # Run the job setup hooks
    for hook in $citbx_job_stage_setup; do
        $citbx_before_script
        $hook
        $citbx_after_script
    done

    CITBX_SCRIPT_COMMON='
        _which() {
            for p in $(echo "$PATH" | tr : \ ); do
                if [ -x "$p/$1" ]; then
                    echo "$p/$1"
                    return 0
                fi
            done
            return 1
        }
        if ! SHELL="$(_which bash || _which sh)"; then
            echo shell not found
            exit 1
        fi
    '

    case "$CITBX_JOB_EXECUTOR" in
        shell)
            (
                unset CITBX
                export GITLAB_CI=true
                for e in ${CITBX_ENV_EXPORT_LIST[@]}; do
                    export $e
                done
                for e in "${CITBX_JOB_VARIABLE_LIST[@]}"; do
                    export $e
                done
                CITBX_SHELL_SCRIPT_PARTS=("$CITBX_SCRIPT_COMMON"'$SHELL')
                if [ "$CITBX_RUN_SHELL" == "true" ]; then
                    print_info "Running a shell within the job \"$CI_JOB_NAME\" environment..."
                else
                    print_info "Running the job \"$CI_JOB_NAME\" into the shell..."
                    CITBX_SHELL_SCRIPT_PARTS+=('-c '"$CITBX_JOB_SCRIPT"'')
                fi
                # Shell selector (like the gitlab-runner one) will try to find bash, otherwise sh
                eval "${CITBX_SHELL_SCRIPT_PARTS[*]}"
            ) || exit 1
            ;;
        docker)
            # Setup docker environment
            if [ -z "$CITBX_JOB_IMAGE_NAME" ] || [ "$CITBX_JOB_IMAGE_NAME" == "null" ]; then
                print_critical "No image property found in .gitlab-ci.yml for the job \"$CI_JOB_NAME\""
            fi
            CITBX_ID=$(head -c 8 /dev/urandom | od -t x8 -An | grep -oE '\w+')
            CITBX_DOCKER_PREFIX="citbx-$CITBX_ID"
            case "$CITBX_DOCKER_AUTH_CONFIG_MODE" in
                environment)
                    citbx_export DOCKER_AUTH_CONFIG
                ;;
                mount-ro|mount-rw)
                    if [ -f "$HOME/.docker/config.json" ]; then
                        CITBX_JOB_DOCKER_RUN_ARGS+=(-v
                            $HOME/.docker/config.json:$HOME/.docker/config.json:${CITBX_DOCKER_AUTH_CONFIG_MODE##mount-})
                        if [ "$CITBX_UID" -ne 0 ]; then
                            CITBX_DOCKER_SCRIPT_PARTS+=("
                                # Fix permission on the $HOME/.docker
                                chown $CITBX_UID:$CITBX_UID $HOME/.docker
                            ")
                        fi
                    fi
                ;;
            esac
            CITBX_DOCKER_SCRIPT_PARTS+=("$CITBX_SCRIPT_COMMON"'
                # PATH environment variable propagation
                echo "ENV_PATH PATH=$PATH" >> /etc/login.defs
                echo "ENV_SUPATH PATH=$PATH" >> /etc/login.defs
                echo "FAKE_SHELL $SHELL" >> /etc/login.defs
            ')
            if [ "$CITBX_UID" -ne 0 ]; then
                CITBX_USER=$USER
                if [ "$CITBX_DOCKER_USER" == "root" ]; then
                    CITBX_DOCKER_SCRIPT_PARTS+=('
                        # _adduser <name> <uid> <home>
                        if _which useradd > /dev/null 2>&1; then
                            _adduser() {
                                useradd -o -u $2 -s /bin/sh -d $3 -M $1
                            }
                        elif _which busybox > /dev/null 2>&1; then
                            _adduser() {
                                busybox adduser -u $2 -s /bin/sh -h $3 -H -D $1
                            }
                        else
                            echo "[!!] No usual tool found to add an user"
                            exit 1
                        fi

                        # _adduser2group <user> <group>
                        if _which usermod > /dev/null 2>&1; then
                            _adduser2group() {
                                usermod -a -G $2 $1
                            }
                        elif _which busybox > /dev/null 2>&1; then
                            _adduser2group() {
                                addgroup $1 $2 > /dev/null
                            }
                        else
                            echo "[!!] No usual tool found to add an user to a group"
                            exit 1
                        fi

                        # Setup user/group
                        CITBX_UID='"$CITBX_UID"'
                        CITBX_USER="$(awk -F : -v uid=$CITBX_UID '\''$3 == uid {print $1}'\'' /etc/passwd)"
                        CITBX_HOME="'"$HOME"'"
                        if [ -z "$CITBX_USER" ]; then
                            CITBX_USER='"$CITBX_USER"'
                            _adduser $CITBX_USER $CITBX_UID "$CITBX_HOME"
                        fi
                        # Fix rights on HOME directory
                        chown $CITBX_UID "$CITBX_HOME"
                        # Add user to the suitable groups
                        for group in '"${CITBX_USER_GROUPS[*]}"'; do
                            if grep -q ^$group /etc/group; then
                                _adduser2group $CITBX_USER $group
                            fi
                        done
                        # Add sudoers suitable rule
                        if [ -f /etc/sudoers ]; then
                            sed -i "/^$CITBX_USER /d" /etc/sudoers
                            echo "$CITBX_USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
                        fi
                        ')
                fi
            else
                CITBX_USER=root
            fi
            # Shell selector (like the gitlab-runner one) will try to find bash, otherwise sh
            CITBX_DOCKER_SCRIPT_PARTS+=('su ${CITBX_USER:-'"$CITBX_USER"'} -s "$SHELL"')
            if [ "$CITBX_RUN_SHELL" != "true" ]; then
                CITBX_DOCKER_SCRIPT_PARTS+=("-c $CITBX_JOB_SCRIPT")
            fi

            if [ -n "$CITBX_DOCKER_USER" ]; then
                CITBX_JOB_DOCKER_RUN_ARGS+=(-u "$CITBX_DOCKER_USER")
            fi

            # Compute the environment variables
            for e in ${CITBX_ENV_EXPORT_LIST[@]}; do
                CITBX_DOCKER_RUN_ARGS+=(-e $e="${!e}")
            done

            CITBX_PRE_COMMANDS=()
            # Entrypoint override management
            if [ ${#CITBX_JOB_IMAGE_ENTRYPOINT[@]} -gt 0 ]; then
                CITBX_JOB_DOCKER_RUN_ARGS+=(--entrypoint "$CITBX_JOB_IMAGE_ENTRYPOINT")
                for e in "${CITBX_JOB_IMAGE_ENTRYPOINT[@]:1}"; do
                    CITBX_PRE_COMMANDS+=("$e")
                done
            fi

            # hook executed on exit
            executor_docker_exit_hook() {
                test -n "$CITBX_DOCKER_PREFIX" || print_critical "Assert: empty CITBX_DOCKER_PREFIX"
                for d in $(docker ps -a --filter "label=$CITBX_DOCKER_PREFIX" -q); do
                    docker rm -f $d > /dev/null 2>&1 || true
                done
            }
            trap executor_docker_exit_hook EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM

            # Start a service
            start_docker_service() {
                local image_name alias entrypoint command
                eval "$1"
                local args=()
                local name=${alias:-$(echo "${image_name%%:*}" | sed -E 's/[^a-zA-Z0-9\._-]/__/g')}
                local ip
                local pattern='\b'"$name"'\b'
                if [[ "${CITBX_DISABLED_SERVICES[*]}" =~ $pattern ]]; then
                    print_note "Skipping $name service start"
                    return 0
                fi
                args+=(--name "$CITBX_DOCKER_PREFIX-$name" --label "$CITBX_DOCKER_PREFIX")
                command_list=()
                if [ ${#entrypoint[@]} -gt 0 ]; then
                    args+=(--entrypoint "$entrypoint")
                    command_list+=("${entrypoint[@]:1}")
                fi
                command_list+=("${command[@]}")
                if [ "$CITBX_SERVICE_DOCKER_PRIVILEGED" == "true" ]; then
                    args+=(--privileged)
                fi
                print_info "Starting service $name..."
                docker run -d "${args[@]}" "${CITBX_DOCKER_RUN_ARGS[@]}" "$image_name" "${command_list[@]}"
                # Get container IP and add --add-host options
                ip=$(docker inspect $CITBX_DOCKER_PREFIX-$name | jq -r .[0].NetworkSettings.Networks.bridge.IPAddress)
                if [ -z "$ip" ]; then
                    docker logs $CITBX_DOCKER_PREFIX-$name
                    print_critical "Unable to get the $name service IP address" \
                                    "This service seems to not be correctly started" \
                                    "=> Try again with --service-privileged option"
                fi
                CITBX_JOB_DOCKER_RUN_ARGS+=(--add-host "$name:$ip")
            }

            # Start services
            local srvs_params
            for srvs_params in "${CITBX_JOB_SERVICE_LIST[@]}"; do
                start_docker_service "$srvs_params"
            done

            # Add project dir mount
            CITBX_JOB_DOCKER_RUN_ARGS+=(-v "$CI_PROJECT_DIR:$CI_PROJECT_DIR:rw")
            local git_dir_path=$(readlink -f $(git rev-parse --git-common-dir))
            CITBX_JOB_DOCKER_RUN_ARGS+=(-v "$git_dir_path:$git_dir_path:rw")

            if [ "$CITBX_JOB_ACCESS_TO_HOST_DOCKER_SOCK_ENABLED" == "true" ]; then
                CITBX_JOB_DOCKER_RUN_ARGS+=(-v /var/run/docker.sock:/var/run/docker.sock)
            fi

            if [ "$CITBX_RUN_SHELL" == "true" ]; then
                print_info "Running a shell into the $CITBX_JOB_IMAGE_NAME docker container..."
                CITBX_JOB_DOCKER_RUN_ARGS+=(-w "$(readlink -f "$PWD")")
            else
                print_info "Running the job \"$CI_JOB_NAME\" into the $CITBX_JOB_IMAGE_NAME docker container..."
                CITBX_JOB_DOCKER_RUN_ARGS+=(-w "$CI_PROJECT_DIR")
            fi

            # Detect if the tool is launched from a terminal
            if tty -s; then
                CITBX_JOB_DOCKER_RUN_ARGS+=(-t)
            fi

            # Run the docker
            docker run --rm -i --name="$CITBX_DOCKER_PREFIX-build" --hostname="$CITBX_DOCKER_PREFIX-build" \
                -e CI=true -e GITLAB_CI=true "${CITBX_DOCKER_RUN_ARGS[@]}" \
                --label "$CITBX_DOCKER_PREFIX" "${CITBX_JOB_DOCKER_RUN_ARGS[@]}" \
                -e DOCKER_RUN_EXTRA_ARGS="$(bashopts_get_def bashopts_extra_args)" "${bashopts_extra_args[@]}" \
                $CITBX_JOB_IMAGE_NAME "${CITBX_PRE_COMMANDS[@]}" sh -c "${CITBX_DOCKER_SCRIPT_PARTS[*]}" \
                || exit $?
            ;;
        *)
            print_critical "Invalid or unsupported '$CITBX_JOB_EXECUTOR' executor"
            ;;
    esac
}

citbx_eval_gitlab_ci() {
    eval "$(
        EXPORT_VAR_LIST=(
            CI_API_V4_URL
            CI_CONFIG_PATH
            CI_JOB_TOKEN
            CI_PROJECT_DIR
            CITBX_HTTP_USER_AGENT
            CITBX_JOB_DEPENDENCY_STRATEGY
            CITBX_JOB_PARALLEL_MATRIX_ID
            CITBX_TEMPLATE_API_URL
            CITBX_TEMPLATE_PROJECT_ID
            CITBX_YAML2BASH_DEBUG
            "$@"
        )
        export "${EXPORT_VAR_LIST[@]}"
        python3 "$CITBX_TOOL_DIR/gitlabci_yml2bash.py"
    )"
}

# display a message
print_log() {
    local level=$1;
    shift || print_log C "Usage print_log <level> message"
    case "${level,,}" in
        c|critical)
            >&2 printf "\033[91m[CRIT] %s\033[0m\n" "$@"
            exit 1
            ;;
        e|error)
            >&2 printf "\033[91m[ERRO] %s\033[0m\n" "$@"
            ;;
        w|warning)
            >&2 printf "\033[93m[WARN] %s\033[0m\n" "$@"
            ;;
        n|note)
            printf "[NOTE] %s\n" "$@"
            ;;
        i|info)
            printf "\033[92m[INFO] %s\033[0m\n" "$@"
            ;;
        *)
            print_log C "Invalid log level: $level"
            ;;
    esac
}
bashopts_log_handler="print_log"
. "$CITBX_TOOL_DIR/3rdparty/bashopts.sh"

if [ "$CITBX_BACKTRACE_ENABLED" == "true" ]; then
    # Enable backtrace display on error
    trap 'bashopts_exit_handle' EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM
fi

if [ "$CITBX_OPTION_INTERACTIVE_MODE_ENABLED_DEFAULT" != "true" ]; then
    # Set non interactive mode by default
    BASHOPTS_SETUP_OPTS+=(-y)
fi

bashopts_setup -n "$(basename ci-toolbox)" "${BASHOPTS_SETUP_OPTS[@]}" \
    -d "CI toolbox for Gitlab-CI (version $CITBX_VERSION)"

if [ "$CITBX_BASHCOMP" == "commands" ]; then
    citbx_eval_gitlab_ci
    printf '"%s"\n' help setup "${CITBX_JOB_LIST[@]}"
    exit 0
fi

command=$1
if ! shift; then
    menu_list=()
    if [ ${#CITBX_MISSING_PKGS[@]} -eq 0 ] && [ -n "$CI_PROJECT_DIR" ]; then
        citbx_eval_gitlab_ci
        for job in "${CITBX_JOB_LIST[@]}"; do
            menu_list+=("$job:Run the job: $job")
        done
    fi
    menu_list+=(
        "help:Display the help"
        "setup:Setup the environment"
        "update:Update this tool"
        ":Exit this tool"
    )
    command="$(citbx_choose "${menu_list[@]}")"
    test -n "$command" || exit 0
fi
case "$command" in
    h|help|-h|--help)
        if [ ${#CITBX_MISSING_PKGS[@]} -eq 0 ] && [ -n "$CI_PROJECT_DIR" ]; then
            citbx_eval_gitlab_ci
        fi
        bashopts_tool_usage="$(
                printf "%s\n"                                                                                         \
                    "ci-toolbox command [command options] [arguments...]"                                             \
                    "  => type 'ci-toolbox command -h' to display the contextual help"                                \
                    "" "COMMANDS:"                                                                                    \
                    "    help       : Display this help"                                                              \
                    "    setup      : Setup the environment"                                                          \
                    "    update     : Update this tool (alias of the command \`setup --component ci-toolbox\`)"       \
                    "    ... or a job from the job list"
                if [ ${#CITBX_JOB_LIST[@]} -gt 0 ]; then
                    printf "\nJOBS:\n"
                    printf "    %s\n" "${CITBX_HELP_JOB_LIST[@]}"
                fi
            )"
        bashopts_display_help_delayed
        ;;
    update)
        bashopts_tool_usage="$(
                echo "ci-toolbox $command [arguments...]"
                echo "  => type 'ci-toolbox help' to display the global help"
            )"
        ;;
    setup)
        bashopts_tool_usage="$(
                echo "ci-toolbox $command [arguments...]"
                echo "  => type 'ci-toolbox help' to display the global help"
            )"
        CITBX_SETUP_COMPONENT_DEFAULT_LIST+=(base-pkgs ca-certs git-lfs)
        bashopts_declare -n CITBX_SETUP_COMPONENT -l component -i \
            -t enum -m add -d "Setup only specified components" \
            -e base-pkgs -e docker-cfg -e git-lfs -e ca-certs -e ci-toolbox \
            -x "(${CITBX_SETUP_COMPONENT_DEFAULT_LIST[*]})"
        bashopts_declare -n CITBX_SETUP_OS_ID -l os-id -i \
            -t string -d "Operating system ID" \
            -v "${OS_RELEASE_INFO[ID]}"
        bashopts_declare -n CITBX_DOCKER_BIP -l docker-bip -i -v "$(
            val=$(jq -r '.bip' /etc/docker/daemon.json 2> /dev/null || true)
            echo ${val:-"192.168.255.254/24"}
        )" -t string -d "Local docker network IPV4 host adress"
        bashopts_declare -n CITBX_DOCKER_FIXED_CIDR -l docker-cdir -i -v "$(
            val=$(jq -r '."fixed-cidr"' /etc/docker/daemon.json 2> /dev/null || true)
            echo ${val:-"192.168.255.0/24"}
        )" -t string -d "Local docker network IPV4 prefix"
        bashopts_declare -n CITBX_DOCKER_DNS_LIST -l docker-dns -i -m add \
            -x "($(
                if [ "0$(jq -e '.dns | length' /etc/docker/daemon.json 2> /dev/null || true)" -gt 0 ]; then
                    jq -r '.dns[]' /etc/docker/daemon.json 2> /dev/null | tr '\n' ' '
                else
                    RESOLV_CONF_DNS="$(cat /etc/resolv.conf | awk '/^nameserver/ {
                        if ($2 !~ /^127\..*/ && $2 != "::1" ) {
                            printf(" %s", $2);
                        }
                    }' 2> /dev/null || true)"
                    echo "${RESOLV_CONF_DNS:-${CITBX_DOCKER_DEFAULT_DNS[*]}}"
                fi
                ) )" \
            -t string -d "Docker DNS"
        bashopts_declare -n CITBX_DOCKER_STORAGE_DRIVER -l docker-storage-driver -i -v "$(
            val=$(jq -r '."storage-driver"' /etc/docker/daemon.json 2> /dev/null || true)
            echo ${val:-"overlay2"}
        )" -e 'o|overlay2' -e 'overlay' -e 'a|aufs' -e 'd|devicemapper' -e 'b|btrfs' -e 'z|zfs' \
            -t enum -d "Docker storage driver"
        bashopts_declare -n CITBX_CA_CERTIFICATES -l ca-certificates -i -t string \
            -d "CA certificate directory with certificates (file names with '.crt' extension) to import"
        ;;
    *)
        if [ -z "$CI_PROJECT_DIR" ]; then
            print_critical "Unable to detect project root directory"
        fi
        if [ ! -e "$CI_PROJECT_DIR/$CI_CONFIG_PATH" ]; then
            print_critical "$CI_PROJECT_DIR/$CI_CONFIG_PATH file not found"
        fi
        : ${CI_REGISTRY:=$DEFAULT_CI_REGISTRY}
        CI_JOB_NAME=$command
        : ${CITBX_DEFAULT_JOB_DEPENDENCY_STRATEGY:="default"}
        : ${CITBX_JOB_DEPENDENCY_STRATEGY:="$CITBX_DEFAULT_JOB_DEPENDENCY_STRATEGY"}

        # Load job
        citbx_eval_gitlab_ci CI_JOB_NAME

        # Define job usage
        bashopts_tool_usage="$(
                echo "ci-toolbox '${command//\'/\\\'}' [arguments...]"
                echo "  => type 'ci-toolbox help' to display the global help"
            )"
        # Define the generic options
        bashopts_declare -n CITBX_JOB_PARALLEL_MATRIX_ID -l matrix -o M \
            -d "Matrix item number for parallel jobs" -t number
        bashopts_declare -n GIT_SUBMODULE_STRATEGY -l submodule-strategy \
            -d "Git submodule strategy (none, normal or recursive)" -t enum -v "${GIT_SUBMODULE_STRATEGY:-none}" \
            -e 'none' -e 'normal' -e 'recursive'
        bashopts_declare -n CITBX_GIT_CLEAN -l git-clean -o c \
            -d "Perfom a git clean -fdx in the main project and submodules" -t boolean
        bashopts_declare -n CITBX_DOCKER_LOGIN_MODE -l docker-login -d "Execute docker login" -t enum \
            -e "enabled" -e "disabled" -e "auto" -v "${CITBX_DEFAULT_DOCKER_LOGIN_MODE:-disabled}"
        bashopts_declare -n CITBX_JOB_EXECUTOR -l job-executor -o e \
            -d "Job executor type (only docker or shell is supported yet)" -t enum \
            -v 'auto' -e 'a|auto' -e 's|shell' -e 'd|docker'
        bashopts_declare -n CI_JOB_TOKEN -l job-token -o T -d "Job token" -t string
        bashopts_declare -n CITBX_RUN_JOB_DEPENDENCIES -l with-dependencies -t boolean \
            -d "Run job dependencies"
        bashopts_declare -n CITBX_JOB_DEPENDENCY_STRATEGY -l dependency-strategy -d "Job dependency strategy" -t enum \
            -e "default" -e "needs" -e "needs:artifacts" -v "${CITBX_DEFAULT_JOB_DEPENDENCY_STRATEGY:-default}"
        bashopts_declare -n CITBX_SUBJOB_ENV_EXPORT_LIST -l export-to-subjob -o E -t string -m add \
            -d "Export a property to a sub job (only applicable if CITBX_RUN_JOB_DEPENDENCIES property/export-to-subjob option is enabled)"
        bashopts_declare -n CITBX_UID -l uid -t number \
            -d "Start this script as a specific uid (0 for root)" -v "$(id -u)"
        CITBX_USER_GROUPS=(adm plugdev)
        bashopts_declare -n CITBX_USER_GROUPS -l group -t string -m add \
            -d "User group list"
        bashopts_declare -n CITBX_DEBUG_SCRIPT_ENABLED -o x -l debug-script -t boolean \
            -d "Enable SHELL script debug (set -x)"
        citbx_export CITBX_DEBUG_SCRIPT_ENABLED
        bashopts_declare -n CITBX_RUN_SHELL -o s -l shell -t boolean \
            -d "Run a shell instead of running the default command (override CITBX_COMMAND option)"
        bashopts_declare -n CITBX_DISABLED_SERVICES -l disable-service -t string \
            -d "Disable a service" -m add
        bashopts_declare -n CITBX_SERVICE_DOCKER_PRIVILEGED -l service-privileged -t boolean \
            -d "Start service docker container in privileged mode" -v "${CITBX_DEFAULT_SERVICE_DOCKER_PRIVILEGED:-false}"
        bashopts_declare -n CITBX_JOB_ACCESS_TO_HOST_DOCKER_SOCK_ENABLED -l access-to-host-docker -t boolean \
            -d "Enable access to the host docker daemon into the job environment" -v "${CITBX_DEFAULT_JOB_ACCESS_TO_HOST_DOCKER_SOCK_ENABLED:-false}"
        bashopts_declare -n DOCKER_AUTH_CONFIG -l docker-auth-config -t string \
            -d "Docker config file (default: $HOME/.docker/config.json) content" -v "$(
                if [ -f "$HOME/.docker/config.json" ]; then
                    jq -c '.' "$HOME/.docker/config.json"
                fi
            )"
        bashopts_declare -n CITBX_DOCKER_AUTH_CONFIG_MODE -l docker-auth-config-mode -t enum -e none -e environment -e mount-ro -e mount-rw \
            -d "Docker configuration can be propagated using DOCKER_AUTH_CONFIG variable, or else by mounting your $HOME/.docker/config.json" \
            -v "${CITBX_DEFAULT_DOCKER_AUTH_CONFIG_MODE:-none}"
        CITBX_DOCKER_USER=${CITBX_DOCKER_USER:-root}

        # Load job specific part
        module_handler_list="define setup"
        if [ -n "$CITBX_JOB_FILE_NAME" ]; then
            citbx_export CITBX_JOB_FILE_NAME
        else 
            CITBX_JOB_FILE_NAME="$CI_JOB_NAME.sh"
        fi
        if [ -f "$CITBX_JOBS_DIR/$CITBX_JOB_FILE_NAME" ]; then
            . "$CITBX_JOBS_DIR/$CITBX_JOB_FILE_NAME"
            citbx_register_handler "job" "define"
            citbx_register_handler "job" "setup"
        fi
        for hook in $citbx_job_stage_define; do
            cd $CI_PROJECT_DIR
            $hook
        done
        ;;
esac

if [ -n "$CITBX_BASHCOMP" ]; then
    # ### BASH completion specific part ###
    # Used only by the bashcomp tool to generate completion words
    case "$CITBX_BASHCOMP" in
        opts)
            for o in "${bashopts_optprop_short_opt[@]}"; do
                echo "\"-$o\""
            done | sort -u
            for o in "${bashopts_optprop_long_opt[@]}"; do
                echo "\"--$o\""
            done | sort -u
            ;;
        longopts)
            for o in "${bashopts_optprop_long_opt[@]}"; do
                echo "\"--$o\""
            done | sort -u
            ;;
        --docker-image)
            while read -r line; do
                echo "\"$line\""
            done <<< "$(docker image ls | tail -n +2 \
                | awk '($1 != "<none>" && $2 != "<none>") {print $1":"$2}')"
            ;;
        --export-to-subjob|-E)
            printf '"%s"\n' "${bashopts_optlist[@]:1}"
            ;;
        -*)
            bashopts_get_valid_value_list $CITBX_BASHCOMP
            ;;
    esac
    exit 0
fi

# Parse arguments
bashopts_parse_args "$@"

# Process argument
bashopts_process_opts

# check the environment
citbx_check_env $(test "$command" != "setup" || echo "true")

case "$command" in
    update)
        $0 setup --component ci-toolbox
    ;;
    *)
        citbx_run_job "$command"
    ;;
esac
