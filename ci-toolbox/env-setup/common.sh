
sudo_sponge() {
    local out=${1:-/dev/stdout}
    _sudo awk -v o="${out}" '
        {
            b = (NR > 1 ? b""ORS""$0 : $0);
        }
        END{
            print b > o;
        }'
}

setup_component_enabled() {
    local pattern='\b'"$1"'\b'
    if [[ "${CITBX_SETUP_COMPONENT[*]}" =~ $pattern ]]; then
        return 0
    fi
    return 1
}

if [ $(id -u) -eq 0 ]; then
    _sudo() {
        >&2 printf "\033[93m[ROOT] %s\033[0m\n" "sudo $*"
        "$@"
    }
else
    if ! which sudo > /dev/null 2>&1; then
        print_critical "In user mode, sudo with suitable system rights is required"
    fi
    _sudo() {
        >&2 printf "\033[93m[ROOT] %s\033[0m\n" "sudo $*"
        sudo "$@"
    }
fi

write_daemon_json() {
    check_dns() {
        case "$1" in
            ::1|127.*)
                print_error "Local $1 DNS server cannot be used with docker containers"
                return 1
                ;;
            *)
                echo "$1"
                ;;
        esac
    }
    bashopts_process_option -n CITBX_DOCKER_DNS_LIST -k check_dns -r

    # Setup docker0 bridge with:
    # - NET: 192.168.255.0/24 (by default)
    # - DNS: Use system dns instead of Google one
    _sudo mkdir -p /etc/docker
    if ! jq '' /etc/docker/daemon.json > /dev/null 2>&1; then
        if [ -f /etc/docker/daemon.json ]; then
            _sudo mv /etc/docker/daemon.json{,.bak}
            print_warning "Invalid file /etc/docker/daemon.json, moving it to /etc/docker/daemon.json.bak"
        fi
        _sudo bash -c 'echo {} > /etc/docker/daemon.json'
    fi
    for dns in $CITBX_DOCKER_DNS_LIST; do
        if [ -n "$dnslist" ]; then
            dnslist="$dnslist, \"$dns\""
        else
            dnslist="\"$dns\""
        fi
    done

    _sudo cat /etc/docker/daemon.json |
    jq '. + {
        "bip": "'"$CITBX_DOCKER_BIP"'",
        "fixed-cidr": "'"$CITBX_DOCKER_FIXED_CIDR"'",
        "dns": '"$(bashopts_dump_array "string" "${CITBX_DOCKER_DNS_LIST[@]}")"',
        "storage-driver": "'"$CITBX_DOCKER_STORAGE_DRIVER"'"
    }' | sudo_sponge /etc/docker/daemon.json
}

install_pkgs_postinst() {
    if [ $(id -u) -ne 0 ]; then
        _sudo usermod -a -G docker $(id -u -n)
    fi
}

CITBX_TMPDIR=$(mktemp -d)
install_finish() {
    rm -rf $CITBX_TMPDIR
}
trap install_finish EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM

fetch_dist_file() {
    wget -O "$1" "$2"
}

fetch_ci_toolbox_pkg() {
    local pkg_type=$1
    local version=${2:-$(curl -s https://gitlab.com/api/v4/projects/mbedsys%2fcitbx4gitlab/releases | jq -r '.[0].tag_name')}
    print_note "Downloading $version archive from gitlab.com..."
    fetch_dist_file "$CITBX_TMPDIR/ci-toolbox.$pkg_type" \
        https://gitlab.com/api/v4/projects/mbedsys%2fcitbx4gitlab/packages/generic/ci-toolbox/$version/ci-toolbox-$version.$pkg_type
}

install_ca_certificates_system() {
    if [ -z "$CITBX_CA_CERTIFICATES" ] || [ ! -d "$CITBX_CA_CERTIFICATES" ]; then
        return 0
    fi
    _sudo mkdir -p /usr/local/share/ca-certificates/
    _sudo cp -v "$CITBX_CA_CERTIFICATES"/* /usr/local/share/ca-certificates/
    _sudo update-ca-certificates
}

install_ca_certificates_docker() {
    if [ -z "$CITBX_CA_CERTIFICATES" ] || [ ! -d "$CITBX_CA_CERTIFICATES" ]; then
        return 0
    fi
    _sudo mkdir -p /etc/docker/certs.d
    _sudo cp -v "$CITBX_CA_CERTIFICATES"/* /etc/docker/certs.d/
}
