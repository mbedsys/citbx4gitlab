
SCRIPT='
echo "URL=$URL"
echo "IMPORTANT_VAR=$IMPORTANT_VAR"
echo "GITLAB=$GITLAB"
echo "IMAGE_NAME=$CITBX_JOB_IMAGE_NAME"
echo "$CITBX_JOB_SCRIPT"
'

case "$CI_JOB_NAME" in
    rspec)
        run_check \
            -m "^URL=http://docker-url.internal$" \
            -m "^IMPORTANT_VAR=the details$" \
            -m "^GITLAB=is-awesome$" \
            -m "^IMAGE_NAME=alpine$" \
            -m "^rake rspec" \
            -- "$SCRIPT"
    ;;
    '')
        run_check \
            -m "^rspec$" \
            -- 'echo ${CITBX_JOB_LIST[*]}'
    ;;
esac
