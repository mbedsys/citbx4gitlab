
SCRIPT='
echo "DIR_HIDDEN=$DIR_HIDDEN"
echo "DIR_BAZ=$DIR_BAZ"
echo "DIR_BAR=$DIR_BAR"
echo "DIR_FOO=$DIR_FOO"
echo "RSPEC_SUITE=$RSPEC_SUITE"
echo "$CITBX_JOB_SCRIPT"
'

case "$CI_JOB_NAME" in
    'rspec 1')
        run_check \
            -m "^DIR_HIDDEN=Seshah3i$" \
            -m "^DIR_BAZ=AoV6zai0" \
            -m "^DIR_BAR=Loo1ohw2$" \
            -m "^DIR_FOO=Oo6vaeth$" \
            -m "^RSPEC_SUITE=1$" \
            -m "^rake rspec" \
            -- "$SCRIPT"
    ;;
    'rspec 2')
        run_check \
            -m "^DIR_HIDDEN=Seshah3i$" \
            -m "^DIR_BAZ=AoV6zai0" \
            -m "^DIR_BAR=Loo1ohw2$" \
            -m "^DIR_FOO=Oo6vaeth$" \
            -m "^RSPEC_SUITE=2$" \
            -m "^rake rspec" \
            -- "$SCRIPT"
    ;;
    'spinach')
        run_check \
            -m "^DIR_HIDDEN=Seshah3i$" \
            -m "^DIR_BAZ=AoV6zai0" \
            -m "^DIR_BAR=Loo1ohw2$" \
            -m "^DIR_FOO=Oo6vaeth$" \
            -m "^rake spinach" \
            -- "$SCRIPT"
    ;;
    '')
        run_check \
            -m "^rspec 1 rspec 2 spinach$" \
            -- 'echo ${CITBX_JOB_LIST[*]}'
    ;;
esac
  
