
SCRIPT1='
echo "CITBX_JOB_PARALLEL_MATRIX_COUNT=$CITBX_JOB_PARALLEL_MATRIX_COUNT"
for i in $(seq 0 $(($CITBX_JOB_PARALLEL_MATRIX_COUNT - 1))); do
    echo "${CITBX_JOB_PARALLEL_MATRIX_LIST[$i]}"
done
'

case "$CI_JOB_NAME" in
    test1)
        run_check \
            -m '^CITBX_JOB_PARALLEL_MATRIX_COUNT=10$' \
            -m '^\* Environment 1:$' \
            -m '^PROVIDER: aws$' \
            -m '^STACK: monitoring$' \
            -m '^$' \
            -m '^\* Environment 2:$' \
            -m '^PROVIDER: aws$' \
            -m '^STACK: app1$' \
            -m '^$' \
            -m '^\* Environment 3:$' \
            -m '^PROVIDER: aws$' \
            -m '^STACK: app2$' \
            -m '^$' \
            -m '^\* Environment 4:$' \
            -m '^PROVIDER: ovh$' \
            -m '^STACK: monitoring$' \
            -m '^$' \
            -m '^\* Environment 5:$' \
            -m '^PROVIDER: ovh$' \
            -m '^STACK: backup$' \
            -m '^$' \
            -m '^\* Environment 6:$' \
            -m '^PROVIDER: ovh$' \
            -m '^STACK: app$' \
            -m '^$' \
            -m '^\* Environment 7:$' \
            -m '^PROVIDER: gcp$' \
            -m '^STACK: data$' \
            -m '^$' \
            -m '^\* Environment 8:$' \
            -m '^PROVIDER: vultr$' \
            -m '^STACK: data$' \
            -m '^$' \
            -m '^\* Environment 9:$' \
            -m '^PROVIDER: gcp$' \
            -m '^STACK: processing$' \
            -m '^$' \
            -m '^\* Environment 10:$' \
            -m '^PROVIDER: vultr$' \
            -m '^STACK: processing$' \
            -- "$SCRIPT1"
    ;;
    test2)
        run_check \
            -m '^aws/app2$' \
            -- 'echo $RESULT'
    ;;
esac
