#!/usr/bin/env bash
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2017-2018 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# Copyright (C) 2018-2020 MBEDSYS - Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

TEST_OK_NB=0
TEST_KO_NB=0
: ${CI_PROJECT_DIR:="$(git rev-parse --show-toplevel 2>/dev/null || true)"}

# Print an error message and exit with error status 1
print_critical() {
    >&2 printf "\033[91m[CRIT] %s\033[0m\n" "$@"
    exit 1
}

# Print a note message
print_note() {
    printf "[NOTE] %s\n" "$@"
}

# Pring an info message
print_info() {
    printf "\033[92m[INFO] %s\033[0m\n" "$@"
}

# Pring an info message
test_msg_ok() {
    printf "\033[92m[<OK>] %s\033[0m\n" "$@"
    ((++TEST_OK_NB))
}

# Print an error message
test_msg_ko() {
    >&2 printf "\033[91m[<!!>] %s\033[0m\n" "$@"
    ((++TEST_KO_NB))
}

test_finish() {
    local retcode=$?
    rm -f output.log
    if [ $retcode -ne 0 ]; then
        print_critical "Exit on fatal error"
    fi
    print_note  "* Test number : $((TEST_OK_NB + TEST_KO_NB))" \
                "* Successes   : $TEST_OK_NB" \
                "* Failures    : $TEST_KO_NB"

    if [ $TEST_KO_NB -eq 0 ]; then
        print_info "Test suite execution succeeded"
    else
        print_critical "Test suite execution failed"
    fi
}

trap test_finish EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM

export CITBX_SEARCH_PATH=ci-toolbox/
export CI_CONFIG_PATH="tests/.gitlab-ci.yml"
export CITBX_SCRIPTS_DIR=tests/ci/scripts

run_test_case() {
    local arglist arg command \
            match_list not_match_list \
            exitcode_c=0 exitcode_m=0 line
    if ! arglist=$(getopt -o "c:m:M:e:" -n "$0 " -- "$@"); then
        print_critical "Usage run_test_case:" \
            "        -j <val>  Job name"
    fi
    eval set -- "$arglist";
    # Store the global bashopts properties
    while true; do
        arg=$1
        shift
        case "$arg" in
            -c) command=$1;              shift;;
            -m) match_list+=("$1");      shift;;
            -e) exitcode_m=$1;           shift;;
            -M) not_match_list+=("$1");  shift;;
            --) break;;
            *)  print_critical "Fatal error";;
        esac
    done

    # Run job
    if [ -z "$command" ]; then
        print_critical "run_test_case: No command specified"
    fi
    $CI_PROJECT_DIR/ci-toolbox/ci-toolbox.sh $command "$@" | tee /tmp/output.log \
        && exitcode_c=${PIPESTATUS[0]} || exitcode_c=${PIPESTATUS[0]} 

    # Check exit code
    if [ $exitcode_c -eq $exitcode_m ]; then
        test_msg_ok "Current exit code ($exitcode_c) match with expected one"
    else
        test_msg_ko "Current exit code ($exitcode_c) don't match with expected one ($exitcode_m)"
    fi

    # Read output
    while read -r line; do
        output_lines+=("$line")
    done <<< "$(sed -E 's/\r//g' /tmp/output.log)"

    # Check matches
    if [ ${#match_list[@]} -gt 0 ]; then
        set -- "${match_list[@]}";
        for line in "${output_lines[@]}"; do
            if [ $# -eq 0 ]; then
                break
            elif [[ "$line" =~ $1 ]]; then
                test_msg_ok "'$1' match as expected - line: $line"
                shift
            fi
        done
        if [ $# -ne 0 ]; then
            test_msg_ko "'$1' don't match as expected or not in the right order"
            shift
            while [ $# -ne 0 ]; do
                test_msg_ko "'$1' match not tested due to previous error"
                shift
            done
        fi
    fi

    for m in "${not_match_list[@]}"; do
        if grep -E "$m" output.log; then
            test_msg_ko "'$m' match and shouldn't"
        else
            test_msg_ok "'$m' don't match as expected"
        fi
    done
}

run_test_case -c job-advanced                               \
    -m 'HOOK example: Job setup.*EXAMPLE_VALUE=EX 1'        \
    -m 'Outside the docker'                                 \
    -m 'JOB_OPTION_LOCAL=JOB!OPT LOCAL$'                    \
    -m 'JOB_OPTION_EXPORT=JOB!OPT EXPORT$'                  \
    -m 'HOOK example: Before the job.*EXAMPLE_VALUE=EX 1'   \
    -m 'Inside the docker'                                  \
    -m 'JOB_OPTION_LOCAL=$'                                 \
    -m 'JOB_OPTION_EXPORT=JOB!OPT EXPORT$'                  \
    -m 'JOB_UNEVALUED_VAR=\$CI_JOB_NAME$'                   \
    -m 'JOB_EVALUED_VAR=job-advanced$'                      \
    -m 'HOOK example: After the job.*exit with code: 0'     \
    --                                                      \
    --example        'EX 1'                                 \
    --job-opt-local  'JOB!OPT LOCAL'                        \
    --job-opt-export 'JOB!OPT EXPORT'

run_test_case -c job-advanced                               \
    -e 133                                                  \
    -m 'HOOK example: After the job.*exit with code: 133'   \
    --                                                      \
    --job-exit-code 133

run_test_case -c minimal-job-global-image                   \
    -m '^TEST_JOB=minimal-job-global-image$'                \
    -m '^Using default image Ubuntu 20.04(.[0-9]+)? LTS$'

run_test_case -c job-with-before-after-script               \
    -m '^executed before$'                                  \
    -m '^script on several$' -m '^lines$' -m '^hi!$'        \
    -m '^executed after$'

run_test_case -c job-with-img-and-custom-ep                 \
    -m '^Job whith image abd custom entrypoint \(TEST_ENV_VAR=OK\)$'

run_test_case -c job-test-services-mysql                    \
    -m '^Waiting for mysql(\.)* done!'                      \
    -m 'version\W+[0-9\.]+'

run_test_case -c job-test-services-postgres                 \
    -m '^Waiting for postgres(\.)* done!'                   \
    -m 'PostgreSQL 9\.4\.[0-9]+ on '

run_test_case -c job-test-deps                              \
    -m 'Running the job \"(job-with-before-after-script|job-advanced)\"' \
    -m 'Running the job \"(job-with-before-after-script|job-advanced)\"' \
    -m 'Running the job \"job-test-deps\"'                  \
    --                                                      \
    --with-dependencies

run_test_case -c job-test-deps                              \
    -m 'JOB_OPTION_EXPORT=dispatched value to subjobs'      \
    -m 'Running the job \"job-test-deps\"'                  \
    --                                                      \
    --with-dependencies
