
citbx_use runshell

CITBX_PACKAGE_NAME="ci-toolbox"

job_define() {
    CITBX_UID=0
}

job_setup() {
    CI_COMMIT_REF_NAME=$(git rev-parse --abbrev-ref HEAD)
    TEST_USER_UID=$(id -u)
    TEST_USER_HOME=$HOME
    citbx_export CI_COMMIT_REF_NAME TEST_USER_UID TEST_USER_HOME
    citbx_docker_run_add_args --privileged
}

job_main() {
    echo "Europe/Paris" > /etc/timezone
    source /etc/os-release
    case "$CI_JOB_NAME" in
        test-package-rpm-*)
            dnf install -y                                                          \
                psmisc                                                              \
                pylint                                                              \
                python3-yaml                                                        \
                sudo
            dnf install -y ./artifacts/ci-toolbox*.rpm
        ;;
        test-package-deb-*)
            apt update
            if [ "$VERSION_ID" == "20.04" ]; then
                PYLINT_PKG=pylint3
            else
                PYLINT_PKG=pylint
            fi
            DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends   \
                psmisc                                                              \
                $PYLINT_PKG                                                             \
                python3-yaml                                                        \
                sudo                                                                \
                tzdata
            apt -y install ./artifacts/ci-toolbox*.deb
        ;;
    esac
    case "$CI_JOB_NAME" in
        *-rel-*)
            export CITBX_PACKAGE_CHANNEL=rel
        ;;
        *-dev-*)
            export CITBX_PACKAGE_CHANNEL=dev
        ;;
    esac
    if [ -z "$TEST_USER_UID" ]; then
        TEST_USER_UID=1000
    fi
    if [ -z "$TEST_USER_HOME" ]; then
        TEST_USER_HOME=/home/testuser
    fi
    
    print_info "Checking python code..."
    if [ -x /usr/bin/pylint ]; then
        pylint_cmd=/usr/bin/pylint
    elif [ -x /usr/bin/pylint3 ]; then
        pylint_cmd=/usr/bin/pylint3
    fi
    print_note " => File ci-toolbox/gitlabci_yml2bash.py"
    python3 -m py_compile ci-toolbox/gitlabci_yml2bash.py
    $pylint_cmd ci-toolbox/gitlabci_yml2bash.py

    print_info "### STEP1 ### Environment setup"
    useradd -o -u $TEST_USER_UID -s /bin/sh -d $TEST_USER_HOME -M test-user
    echo "test-user ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
    if [ ! -d "$TEST_USER_HOME" ]; then
        mkdir -p "$TEST_USER_HOME"
        chown test-user:test-user "$TEST_USER_HOME"
    fi

    print_info "### STEP2 ### ci-toolbox setup"
    sudo -u test-user env HOME="$TEST_USER_HOME" \
        ci-toolbox setup "$CI_COMMIT_REF_NAME"   \
        --docker-storage-driver overlay2         \
        --docker-bip "172.30.0.1/24"             \
        --docker-cdir "172.30.0.0/24"            \
        --docker-dns 9.9.9.9                     \
        --component base-pkgs                    \
        --component docker-cfg                   \
        --component git-lfs                      \
        --component ca-certs

    sudo -u test-user env HOME="$TEST_USER_HOME" \
        git config --global --add safe.directory '*'

    print_info "### STEP3 ### Docker daemon start"
    dockerd --data-root "$CI_PROJECT_DIR/testdir/docker/data" --iptables=false&
    echo -n "Waiting for docker daemon"
    until [ -e /var/run/docker.sock ]; do sleep 1; echo -n "."; done

    print_info "### STEP4 ### run testsuites"
    sudo -u test-user env CI_PROJECT_DIR="$CI_PROJECT_DIR" \
        HOME="$TEST_USER_HOME" ./tests/run-testsuite.sh
    sudo -u test-user env CI_PROJECT_DIR="$CI_PROJECT_DIR" \
        HOME="$TEST_USER_HOME" ./tests/yaml2bash/run-tests.sh
}

job_after() {
    if [ -f /var/run/docker.pid ]; then
        # Kill the docker daemon
        local pid=$(cat /var/run/docker.pid)
        kill $pid || true
        wait $pid || true
    fi
    # Remove testdir and ignore errors
    rm -rf testdir || true
}
